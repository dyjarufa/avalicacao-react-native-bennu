import React,  { useState } from 'react';

import Routes from './src/routes';

import firebase from 'react-native-firebase';

import { useNavigation } from '@react-navigation/native'

import { StyleSheet, Text, View, TextInput, Button, Alert, ActivityIndicator} from 'react-native';


 
export default function App(){

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isAuthenticate, setIsAuthenticate] = useState(false);

  const navigation = useNavigation();

 
  function navigateToTask(task){
    navigation.navigate('Lista de Tarefas');
  }
  


return(
    // <Routes />


    <View style={styles.container}>  
   
    <TextInput
      style={styles.inputStyle}
      placeholder="Email"
      onChangeText={text => setEmail( text )}
      defaultValue={email}
    />
    <TextInput
      style={styles.inputStyle}
      placeholder="Password"
      onChangeText={text => setPassword( text )}
      defaultValue={password}
      maxLength={15}
      secureTextEntry={true}
    />   
    <Button
      color="#3740FE"
      title="Entrar"
      onPress={navigateToTask}
    />

                            
  </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    padding: 35,
    backgroundColor: '#fff'
  },
  inputStyle: {
    width: '100%',
    marginBottom: 15,
    paddingBottom: 15,
    alignSelf: "center",
    borderColor: "#ccc",
    borderBottomWidth: 1
  },
  loginText: {
    color: '#3740FE',
    marginTop: 25,
    textAlign: 'center'
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff'
  }
});