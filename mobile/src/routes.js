import React from 'react';
import {NavigationContainer} from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack';
import { Appbar    } from 'react-native-paper';
const appStack =  createStackNavigator();

import Tasks from './pages/Task';
import EditTask from './pages/EditTask';
import AddTask from './pages/AddTask'


export default function Routes(){
  return (
    <NavigationContainer>
      <appStack.Navigator headerMode="none">
        <appStack.Screen name="Lista de Tarefas" component={Tasks} />
        <appStack.Screen name="Edição da Tarefa" component={EditTask}/>
        <appStack.Screen name="Adicionar nova Tarefa" component={AddTask}/>
      </appStack.Navigator>
    </NavigationContainer>


  );
}
