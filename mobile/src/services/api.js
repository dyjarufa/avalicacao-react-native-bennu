import axios from 'axios';

const api = axios.create({
  baseURL: 'http://192.168.1.5:3333/api'
  // baseURL: 'http://10.0.2.2:3333/api'

});

export default api;
