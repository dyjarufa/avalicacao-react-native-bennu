import React, {useState} from 'react';
import { setHours,getHours, setDate,getDate,setMinutes,getMinutes, parseISO,format} from 'date-fns'
import {View, Text, TouchableOpacity} from 'react-native';
import { useNavigation,  } from '@react-navigation/native';
import { Avatar, Card, Title, Paragraph,TextInput ,Appbar ,Checkbox ,Button   } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialIcons';
import styles from "../AddTask/styles";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import api from "../../services/api";
import qs from 'qs';


export default function AddTask(){
    const navigation = useNavigation();
    const [mark, setChecked] = useState(false);
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const [isTimePickerVisible, setTimePickerVisibility] = useState(false);
    const [dataDeConclusao, setDataDeConclusao] = useState(Date.now());
    const [dataExibicao, setDataExibicao] = useState();
    const [horaExibicao, setHoraExibicao] = useState();
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");


    function navigateBack(){
        navigation.goBack(); //função do próprio useNavigate
    }
    const   _goBack = () => console.log('Went back');

    const    _handleSearch = () => console.log('Searching');

    const  _handleMore = () => console.log('Shown more');


     function Submit() {
        let texto = title;
        let fomulario =  {
            title,
            description,
            "dataAlteracao" : dataDeConclusao,
            "complete":false

        }

        // console.warn("obj :" + qs.stringify(fomulario));
        try {
            let res = api.post("Tasks",fomulario,
                {
                    headers: { "Content-Type": "application/json; charset=utf-8"}}
            ).then((res) => {
                console.log("RESPONSE RECEIVED: ", res);
            })
                .catch((err) => {
                    console.log("AXIOS ERROR: ", err);
                });

            console.log(res);
            console.warn("gravou " );
            navigateBack();
        }catch (e) {
            console.warn("nao gravou " + e);
        }

    }

    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };
    const handleConfirm = date => {
        console.log("Data escolhida ", date.date);
        let dataAux = dataDeConclusao;
        let dataAlterada = setDate(dataAux, getDate(date)   );
        setDataDeConclusao(dataAlterada);
        const dataFormatada = format(
            dataDeConclusao,
            " dd '/' MM '/' yyyy"
        );

        setDataExibicao(dataFormatada);

        console.log("data escolhida : ", dataDeConclusao);
        hideDatePicker();
    };


// hour
    const handleConfirmTime = date => {
        console.log(" horario marcado  ", date);

        let dataAux = dataDeConclusao;

        let dataHora = setHours(dataAux, getHours(date)   );

        dataHora = setMinutes(dataHora,getMinutes(date))

        setDataDeConclusao(dataHora);

        const horaFormatada = format(
            dataDeConclusao,
            " HH ':' mm"
        );

        setHoraExibicao(horaFormatada)

        console.log("horario alterado: ", dataDeConclusao);
        hideTimePicker();
    };

    const showTimePicker = () => {
        setTimePickerVisibility(true);
    };

    const hideTimePicker = () => {
        setTimePickerVisibility(false);
    };


    return (
        <>
            <Appbar.Header>
                <Appbar.BackAction onPress={navigateBack} />
                <Appbar.Content
                    title="Cadastro tarefa"

                />{/* <Appbar.Action icon="dots-vertical" onPress={_handleMore} />*/}
            </Appbar.Header>

            <View style={styles.container}>
                <TextInput    style={styles.text}
                              label='Titulo'
                              onChangeText={text => setTitle( text )}
                />

                <TextInput style={styles.text}
                           label='Descrição'
                           onChangeText={text => setDescription( text )}
                />


                <View style={styles.inlineButton}>
                    <TextInput
                        value={dataExibicao}
                        style={styles.textData}
                        label='Data'
                        disabled
                    />

                    <Button style={styles.buttonImage} icon="calendar"
                            mode="text" onPress={showDatePicker}>

                    </Button>

                    <DateTimePickerModal
                        isVisible={isDatePickerVisible}
                        mode="date"
                        locale="pt_BR"
                        onConfirm={handleConfirm}
                        onCancel={hideDatePicker}
                    />
                </View>

                <View style={styles.inlineButton}>
                    <TextInput
                        value={horaExibicao}
                        style={styles.textData}
                        label='Hora'
                        disabled
                    />

                    <Button style={styles.buttonImage} icon="watch"
                            mode="text" onPress={showTimePicker}>
                    </Button>

                    <DateTimePickerModal
                        isVisible={isTimePickerVisible}
                        mode="time"
                        label="hora"
                        locale="pt_BR"
                        onConfirm={handleConfirmTime}
                        onCancel={hideTimePicker}
                    />
                </View>

                <Checkbox style={styles.text}
                          status={mark ? 'checked' : 'unchecked'}
                          onPress={() => { setChecked({ checked: mark }); }}
                          
                />
                <Button style={styles.buttonFooter} icon="check"  mode="contained"  onPress={Submit }>
                    Salvar
                </Button>
            </View>
        </>
    );



}
