import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 24,
        paddingTop: 20,
        backgroundColor:'#F4F5F7'
    },
    text:{
        backgroundColor: '#F4F5F7',
        marginBottom:10
    },

    textData:{
        backgroundColor: '#F4F5F7',
        marginBottom:10,
        width:150
    },

    inlineButton: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 16,
        justifyContent: 'space-between'

    },
    buttonImg:{
        width: 5
    },
    buttonRedondo: {
        position: "absolute",
        borderRadius: 50,
        width: 60,
        height: 60,
        bottom: 40,
        right: 40,
        alignItems: 'center',
        justifyContent: "center",

    },
    buttonFooter: {
        marginTop: 150
    },


});
