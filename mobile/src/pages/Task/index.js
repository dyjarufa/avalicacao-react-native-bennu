import React, {useEffect, useState} from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { useNavigation } from '@react-navigation/native'

import api from '../../services/api'

import { View, FlatList, Image, Text, TouchableOpacity, Button} from 'react-native';

// import logoImg from '../../assets/logo.png';

import styles from './styles'
import {Appbar} from "react-native-paper";

export default function Tasks(){
  const [tasks, setTasks] = useState([]);
  const navigation = useNavigation();

  function navigateToDeatail(task){
    navigation.navigate('Edição da Tarefa', {task});
  }

  function navigateToAddTask(){
    navigation.navigate('Adicionar nova Tarefa');
  }

  async function handleDeleteIncidente(id){
    try {
     
      await api.delete(`tasks/${id}`);

      setTasks(tasks.filter(task => task._id !== id));
    } catch (error) {
      alert('Erro ao deletar caso, tente novamente')        ;
    }
  }

  useEffect(() =>{
   
    try {
      api.get('/tasks').then(response => {
        console.log(response.data)
        setTasks(response.data);
      })
    } catch (err) {
      console.log(err)
    }

  }, [tasks]);

  return (
<>
      <Appbar.Header>

          <Appbar.Content
              title="Listar Tarefa"
          />
          <Appbar.Action icon="dots-vertical" />
      </Appbar.Header>
    <View style={styles.container}>


     <View style={styles.header}>
       {/* <Image source={logoImg}/>  */}
       <Text style={styles.headerText}></Text>
     </View>

      <FlatList
        data={tasks}
        keyExtractor={task => String(task)}
        showsVerticalScrollIndicator={false}
        renderItem={({ item: task }) =>(
          <View style={styles.task}>
            <Text style={styles.title}>{task.title}</Text>
            <Text style={styles.description}>{task.description}</Text>
            <Text style={styles.description}>{task.dataAlteracao}</Text>

            <TouchableOpacity
                style={styles.deleteButton}
                onPress={() => handleDeleteIncidente(task._id)}
                >
                <Icon name="delete"size={16} color="#9370DB"/>
            </TouchableOpacity>

            <TouchableOpacity
                style={styles.editButton}
                onPress={() => navigateToDeatail(task)}
                >
                <Text style={styles.editButtonText}>Editar Tarefa</Text>
                <Icon name="edit"size={16} color="#9370DB"/>
            </TouchableOpacity>
            
          </View>
        )}
      />


      <View style={styles.actions}>
          <TouchableOpacity style={styles.action} onPress={navigateToAddTask}>
            <Icon name="add" size={16} color="#fff"/>
          </TouchableOpacity>
      </View>

   </View>
    </>
  );
}
