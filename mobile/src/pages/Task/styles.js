import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 24,
    paddingTop: 20,
    backgroundColor:'#FAFAFA'
  },

  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },

  headerText: {
    fontSize: 15,
    color: '#737380',
  },

  headerTextBold: {
    fontWeight: 'bold'
  },

  title: {
    fontSize: 30,
    color: '#13131a',
    fontWeight: 'bold'
  },

  description: {
    fontSize: 16,
    lineHeight: 24,
    color: '#737380'
  },

  task: {
    padding: 24,
    borderRadius: 8,
    backgroundColor: '#fff',
    marginBottom:16
  },


  editButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 16
  },

  editButtonText: {
    color: "#7159c1",
    fontSize: 15,
    fontWeight: 'bold'
  },

  deleteButton: {
    position: "absolute",
    right: 24,
    top: 24,
  },

  action: {
    position: "absolute",
    backgroundColor: '#7159c1',
    borderRadius: 50,
    width: 60,
    height: 60,
    bottom: 40,
    right: 40,
    alignItems: 'center',
    justifyContent: "center",

  },

  actionText: {
    color: "#fff",
    fontSize: 15,
    fontWeight: 'bold'
  }

});
