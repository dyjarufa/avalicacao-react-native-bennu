import React, {useState ,} from 'react';
import { View, Text } from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/native';
import { setHours,getHours, setDate,getDate,setMinutes,getMinutes, parseISO,format} from 'date-fns'
import { Avatar, Card, Title, Paragraph,TextInput ,Appbar ,Checkbox ,Button } from 'react-native-paper';
import styles from "./styles";
import DateTimePickerModal from "react-native-modal-datetime-picker";

import api from "../../services/api";
import qs from 'qs';

export default function Detail(){

  const navigation = useNavigation();
  const route = useRoute();
  const task = route.params.task;

  const [marcao, setChecked] = useState(false);
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [isTimePickerVisible, setTimePickerVisibility] = useState(false);
  const [dataDeConclusao, setDataDeConclusao] = useState(Date.now());
  const [dataExibicao, setDataExibicao] = useState(formataData());
  const [horaExibicao, setHoraExibicao] = useState(formataHora());
  const [title, setTitle] = useState(task.title);
  const [description, setDescription] = useState(task.description);

  function navigateBack(){
    navigation.goBack(); 
  }

  function formataData(){
    const dataFormatada = format(
        dataDeConclusao,
        " dd '/' MM '/' yyyy"
    );
    return dataFormatada;
  }

  function formataHora(){
    const horaFormatada = format(
        dataDeConclusao,
        " HH ':' mm"
    );
    return horaFormatada;
  }

  function Submit() {

    let fomulario =  {
            title,
            description,
           "dataAlteracao": dataDeConclusao,
            "complete":true
    }
    try {
        
        let res = api.put(`tasks/${task._id}`, fomulario,{
                headers: { "Content-Type": "application/json; charset=utf-8"}}
        ).then((res) => {
            console.log("RESPONSE RECEIVED: ", res);
        }).catch((err) => {
                console.log("AXIOS ERROR: ", err);
            });
        console.log(res);
        navigateBack();
    }catch (e) {
        console.log("nao gravou " + e);
    }
    };

      const showDatePicker = () => {
        setDatePickerVisibility(true);
      };

      const hideDatePicker = () => {
          setDatePickerVisibility(false);
      };

      const handleConfirm = date => {
        console.log("Data escolhida ", date.date);
        let dataAux = dataDeConclusao;
        let dataAlterada = setDate(dataAux, getDate(date)   );
        setDataDeConclusao(dataAlterada);
        const dataFormatada = format(
            dataDeConclusao,
            " dd '/' MM '/' yyyy"
        );

        setDataExibicao(dataFormatada);
        hideDatePicker();
    };

    const handleConfirmTime = date => {

      let dataAux = dataDeConclusao;

      let dataHora = setHours(dataAux, getHours(date)   );

      dataHora = setMinutes(dataHora,getMinutes(date))

      setDataDeConclusao(dataHora);

      const horaFormatada = format(
          dataDeConclusao,
          " HH ':' mm"
      );

      setHoraExibicao(horaFormatada)

      console.log("horario alterado: ", dataDeConclusao);
      hideTimePicker();
    };

    const showTimePicker = () => {
      setTimePickerVisibility(true);
    };

    const hideTimePicker = () => {
        setTimePickerVisibility(false);
    };


  return (
    <>
       <Appbar.Header>
          <Appbar.BackAction onPress={navigateBack} />
          <Appbar.Content
              title="Editar tarefa"

          />
      </Appbar.Header>
    
      <View style={styles.container}>

          <TextInput    
            style={styles.text}
            label='Titulo'
            onChangeText={text => setTitle(text)}
            defaultValue={task.title}
          />

          <TextInput 
              style={styles.text}
              label='Descrição'
              onChangeText={text => setDescription(text)}
              defaultValue={task.description}
          />


          <View style={styles.inlineButton}>
              <TextInput
                  // value={dataExibicao ? dataExibicao : task.dataAlteracao}
                  value={dataExibicao}
                  style={styles.textData}
                  label='Data'
                  disabled
              />

              <Button style={styles.buttonImage} icon="calendar"
                      mode="text" onPress={showDatePicker}>

              </Button>

              <DateTimePickerModal
                  isVisible={isDatePickerVisible}
                  mode="date"
                  locale="pt_BR"
                  onConfirm={handleConfirm}
                  onCancel={hideDatePicker}
              />
          </View>

          <View style={styles.inlineButton}>
              <TextInput
                  value={horaExibicao}
                  style={styles.textData}
                  label='Hora'
                  disabled
              />

              <Button style={styles.buttonImage} icon="watch"
                      mode="text" onPress={showTimePicker}>
              </Button>

              <DateTimePickerModal
                  isVisible={isTimePickerVisible}
                  mode="time"
                  label="hora"
                  locale="pt_BR"
                  onConfirm={handleConfirmTime}
                  onCancel={hideTimePicker}
              />
          </View>

          <Checkbox style={styles.text}
                    status={marcao ? 'checked' : 'unchecked'}
                    onPress={() => { setChecked({ checked: marcao }); }}
          />
          <Button style={styles.buttonFooter} icon="pencil"  mode="contained"  onPress={Submit}>
              Editar
          </Button>
      </View>
    </>
    
  );
}