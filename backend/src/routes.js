const express = require('express');

const TasksControllers = require('./controllers/TasksControllers')

const routes = express.Router();

routes.get('/tasks', TasksControllers.index);
routes.get('/tasks/:id', TasksControllers.detail);
routes.put('/tasks/:id', TasksControllers.update);
routes.post('/tasks', TasksControllers.store);
routes.delete('/tasks/:id', TasksControllers.delete);

module.exports = routes;