const mongoose = require('mongoose');

const Task = mongoose.model('Task');

module.exports = {
  async index(request, response){

    const tasks =  await Task.find({});
    return response.json(tasks);
  },

  async store(request, response) {

      const task = await Task.create(request.body);
  
      return response.json(task);
    
  },

  async detail(request, response){
    const task = await Task.findById(request.params.id);

    response.json(task);
  },
  
  async update(request, response) {
    const task = await Task.findByIdAndUpdate(request.params.id, request.body, {
      new: true
    });

    console.log(task);

    return response.json( task );
  },

  async delete( request, response){
    await Task.findByIdAndRemove(request.params.id);

    return response.send()
  }
}